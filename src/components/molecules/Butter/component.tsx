import styled from '@emotion/styled';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classNames from 'classnames';
import React, { FC } from 'react';

import { Props } from './props';

const ButterBase: FC<Props> = ({ className, colors: _a, children, ...rest }: Props) => (
  <div className={classNames(className, 'butter')} {...rest}>
    <span className="butter__title">{children}</span>
    <FontAwesomeIcon icon={faBars} />
  </div>
);

export const Butter = styled(ButterBase)<Pick<Props, 'colors'>>`
  cursor: pointer;
  display: flex;
  align-items: center;
  opacity: 0.8;
  color: ${({ colors }) => colors.textColor};
  transition: opacity 0.2s, color 0.2s;
  &:hover {
    opacity: 1;
  }
  &:hover i {
    transform: rotate(180deg);
  }
  .butter__title {
    font-weight: 600;
    text-transform: uppercase;
    margin-right: 10px;
  }
  i {
    font-size: 20px;
    transition: transform 0.2s;
  }
`;
