import styled from '@emotion/styled';
import classNames from 'classnames';
import { FC } from 'react';
import * as React from 'react';

import { Switch } from '../../atoms/Switch';
import { Props } from './props';

const ThemeSwitchBase: FC<Props> = ({
  className,
  children,
  colors,
  isActive,
  onClick,
  ...rest
}: Props) => (
  <div className={classNames(className, 'theme-switcher-component')} {...rest}>
    <span className="theme-switcher__title">{children}</span>
    <Switch
      colors={colors}
      className={classNames('ml-auto')}
      isActive={isActive}
      onClick={onClick}
    />
  </div>
);

export const ThemeSwitch = styled(ThemeSwitchBase)`
  display: flex;
  align-items: center;
  padding: 10px;
  background: rgba(255, 255, 255, 0.04);
  border-radius: 100px;
  .theme-switcher__title {
    display: block;
    margin-left: 10px;
    margin-right: 40px;
    color: #fff;
    font-weight: 600;
    font-size: 17px;
    text-transform: uppercase;
  }
`;
