import { ColorProps } from '../../../../../core/colors';
import { AnchorProps } from '../../../../atoms/Anchor';

export type Props = AnchorProps & { readonly colors: ColorProps };
