/** @jsx jsx */
import { jsx } from '@emotion/core';
import classNames from 'classnames';
import React, { FC } from 'react';

import { colors, colorsLight } from '../../../core/colors';
import { AnchorButton } from '../../atoms/AnchorButton';
import { Container } from '../../atoms/Container';
import { Heading } from '../../atoms/Heading';
import { Grid, GridItem } from '../Grid';
import { PostCard } from './component';
import { Author } from './libs/Author';
import { Body } from './libs/Body';
import { Cover } from './libs/Cover';
import { Published } from './libs/Published';

export default {
  title: 'Molecules/PostCard',
};

export const DarkTheme: FC = () => (
  <React.Fragment>
    <Container mini>
      <PostCard colors={colors}>
        <Cover url="https://api.ifkz.org/uploads/post/cover/55/photo_2020-06-26_20-10-02.jpg" />
        <Body>
          <Heading className={classNames('mt-0', 'mb-2')} as="h3">
            <AnchorButton className="d-block" css={{ color: 'inherit' }}>
              ОНЛАЙН-АКТИВИЗМ И ЦИФРОВЫЕ ПРАВА
            </AnchorButton>
          </Heading>
          <Author className="mr-2" href="https://t.me/kabyshman" css={{ color: colors.accentBlue }}>
            Елжан Кабышев
          </Author>
          <Published colors={colors}>26.06.2020 09:32</Published>
        </Body>
      </PostCard>
    </Container>
  </React.Fragment>
);

export const LightTheme: FC = () => (
  <React.Fragment>
    <Container mini>
      <PostCard colors={colorsLight}>
        <Cover url="https://api.ifkz.org/uploads/post/cover/55/photo_2020-06-26_20-10-02.jpg" />
        <Body>
          <Heading className={classNames('mt-0', 'mb-2')} as="h3">
            <AnchorButton className="d-block" css={{ color: 'inherit' }}>
              ОНЛАЙН-АКТИВИЗМ И ЦИФРОВЫЕ ПРАВА
            </AnchorButton>
          </Heading>
          <Author href="https://t.me/kabyshman" css={{ color: colorsLight.accentBlue }}>
            Елжан Кабышев
          </Author>
          <Published colors={colorsLight}>26.06.2020 09:32</Published>
        </Body>
      </PostCard>
    </Container>
  </React.Fragment>
);

export const Feed: FC = () => (
  <React.Fragment>
    <Container>
      <Grid columns={12} gap="20px">
        <GridItem columns={12} rowColumns={3}>
          <PostCard colors={colorsLight}>
            <Cover url="https://api.ifkz.org/uploads/post/cover/55/photo_2020-06-26_20-10-02.jpg" />
            <Body>
              <Heading className={classNames('mt-0', 'mb-2')} as="h3">
                <AnchorButton className="d-block" css={{ color: 'inherit' }}>
                  ОНЛАЙН-АКТИВИЗМ И ЦИФРОВЫЕ ПРАВА
                </AnchorButton>
              </Heading>
              <Author href="https://t.me/kabyshman" css={{ color: colorsLight.accentBlue }}>
                Елжан Кабышев
              </Author>
              <Published colors={colorsLight}>26.06.2020 09:32</Published>
            </Body>
          </PostCard>
        </GridItem>
        <GridItem columns={12} rowColumns={3}>
          <PostCard colors={colorsLight}>
            <Cover url="https://api.ifkz.org/uploads/post/cover/55/photo_2020-06-26_20-10-02.jpg" />
            <Body>
              <Heading className={classNames('mt-0', 'mb-2')} as="h3">
                <AnchorButton className="d-block" css={{ color: 'inherit' }}>
                  ОНЛАЙН-АКТИВИЗМ И ЦИФРОВЫЕ ПРАВА
                </AnchorButton>
              </Heading>
              <Author href="https://t.me/kabyshman" css={{ color: colorsLight.accentBlue }}>
                Елжан Кабышев
              </Author>
              <Published colors={colorsLight}>26.06.2020 09:32</Published>
            </Body>
          </PostCard>
        </GridItem>
        <GridItem columns={12} rowColumns={3}>
          <PostCard colors={colorsLight}>
            <Cover url="https://api.ifkz.org/uploads/post/cover/55/photo_2020-06-26_20-10-02.jpg" />
            <Body>
              <Heading className={classNames('mt-0', 'mb-2')} as="h3">
                <AnchorButton className="d-block" css={{ color: 'inherit' }}>
                  ОНЛАЙН-АКТИВИЗМ И ЦИФРОВЫЕ ПРАВА
                </AnchorButton>
              </Heading>
              <Author href="https://t.me/kabyshman" css={{ color: colorsLight.accentBlue }}>
                Елжан Кабышев
              </Author>
              <Published colors={colorsLight}>26.06.2020 09:32</Published>
            </Body>
          </PostCard>
        </GridItem>

        <GridItem columns={12} rowColumns={2}>
          <PostCard colors={colorsLight}>
            <Cover url="https://api.ifkz.org/uploads/post/cover/55/photo_2020-06-26_20-10-02.jpg" />
            <Body>
              <Heading className={classNames('mt-0', 'mb-2')} as="h3">
                <AnchorButton className="d-block" css={{ color: 'inherit' }}>
                  ОНЛАЙН-АКТИВИЗМ И ЦИФРОВЫЕ ПРАВА
                </AnchorButton>
              </Heading>
              <Author href="https://t.me/kabyshman" css={{ color: colorsLight.accentBlue }}>
                Елжан Кабышев
              </Author>
              <Published colors={colorsLight}>26.06.2020 09:32</Published>
            </Body>
          </PostCard>
        </GridItem>
        <GridItem columns={12} rowColumns={2}>
          <PostCard colors={colorsLight}>
            <Cover url="https://api.ifkz.org/uploads/post/cover/55/photo_2020-06-26_20-10-02.jpg" />
            <Body>
              <Heading className={classNames('mt-0', 'mb-2')} as="h3">
                <AnchorButton className="d-block" css={{ color: 'inherit' }}>
                  ОНЛАЙН-АКТИВИЗМ И ЦИФРОВЫЕ ПРАВА
                </AnchorButton>
              </Heading>
              <Author href="https://t.me/kabyshman" css={{ color: colorsLight.accentBlue }}>
                Елжан Кабышев
              </Author>
              <Published colors={colorsLight}>26.06.2020 09:32</Published>
            </Body>
          </PostCard>
        </GridItem>
      </Grid>
    </Container>
  </React.Fragment>
);
