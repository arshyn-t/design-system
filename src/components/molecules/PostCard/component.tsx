import classNames from 'classnames';
import React, { FC } from 'react';

import { Card } from '../Card';
import { Props } from './props';

export const PostCard: FC<Props> = ({ colors, className, children, ...rest }: Props) => {
  return (
    <Card
      className={classNames(className, 'd-flex', 'flex-column', 'p-0')}
      colors={colors}
      {...rest}
    >
      {children}
    </Card>
  );
};
