import { HTMLAttributes } from 'react';

import { ColorProps } from '../../../../../core/colors';

export type Props = HTMLAttributes<HTMLSpanElement> & {
  readonly colors: ColorProps;
};
