import { Props } from './props';

export * from './component';

export type AuthorProps = Props;
