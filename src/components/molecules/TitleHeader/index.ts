import { Props } from './props';

export * from './component';

export type TitleHeaderProps = Props;
