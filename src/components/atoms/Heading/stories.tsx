import React, { FC } from 'react';

import { colors, colorsLight } from '../../../core/colors';
import { Container } from '../Container';
import { MockBackground } from '../MockBackground';
import { Heading } from './component';

export default {
  title: 'Atoms/Heading',
};

export const Headings: FC = () => (
  <>
    <Container mini>
      <MockBackground background={colorsLight.globalBg} color={colorsLight.textColor}>
        <Heading as="h1">This is H1</Heading>
        <Heading as="h2">This is H2</Heading>
        <Heading as="h3">This is H3</Heading>
        <Heading as="h4">This is H4</Heading>
        <Heading as="h5">This is H5</Heading>
        <Heading as="h6">This is H6</Heading>
        <Heading as="span">This is span</Heading>
      </MockBackground>
    </Container>
  </>
);

export const HeadingsColored: FC = () => (
  <>
    <Container mini>
      <MockBackground mini background={colors.globalBg}>
        <Heading as="h1" color={colors.textColor}>
          This is H1
        </Heading>
        <Heading as="h2" color={colors.textColor}>
          This is H2
        </Heading>
        <Heading as="h3" color={colors.textColor}>
          This is H3
        </Heading>
        <Heading as="h4" color={colors.textColor}>
          This is H4
        </Heading>
        <Heading as="h5" color={colors.textColor}>
          This is H5
        </Heading>
        <Heading as="h6" color={colors.textColor}>
          This is H6
        </Heading>
      </MockBackground>
    </Container>
  </>
);
