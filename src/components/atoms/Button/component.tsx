import styled from '@emotion/styled';
import React, { FC } from 'react';

import { Props } from './props';

const IconWrapper = styled.div`
  position: absolute;
  right: -20px;
  opacity: 0;
  top: 50%;
  transform: translateY(-50%);
  transition: right 0.2s, opacity 0.2s;
  will-change: transition, opacity;
  button:hover & {
    right: 20px;
    opacity: 1;
  }
`;

const ButtonBase: FC<Props> = ({ icon, children, colors: _a, ...rest }: Props) => (
  <button {...rest}>
    {children}
    <IconWrapper>{icon}</IconWrapper>
  </button>
);

export const Button = styled(ButtonBase)<Pick<Props, 'colors' | 'icon'>>`
  position: relative;
  background: ${({ colors }) => colors.accentBlue};
  display: inline-block;
  color: #fff;
  border: none;
  border-radius: 100px;
  outline: none;
  cursor: pointer;
  font-family: inherit;
  padding: 20px 30px;
  font-size: 15px;
  text-transform: uppercase;
  font-weight: 600;
  height: 100%;
  overflow: hidden;
  transition: background 0.2s, padding-right 0.2s, opacity 0.2s, padding-right 0.2s;
  will-change: background, padding-right, opacity, padding-right;
  &:hover {
    background: ${({ colors }) => colors.accentBlueHover};
    ${({ icon }) =>
      icon &&
      `
      padding-right: 50px;
    `}
  }
  &:active {
    opacity: 0.8;
  }
`;
