import React, { FC, useState } from 'react';

import {
  Anchor,
  Butter,
  LocaleSelect,
  MockBackground,
  Navigation,
  NavigationItem,
  ThemeSwitch,
} from '../../../components';
import { colors } from '../../../core/colors';
import { Header } from './component';
import { Logo } from './libs/Logo';

export default {
  title: 'Organisms/Header',
};

export const HeaderBasic: FC = () => {
  const languages = {
    ru: 'https://ifkz.org/src/assets/locales/ru.png',
    en: 'https://ifkz.org/src/assets/locales/en.png',
    kk: 'https://ifkz.org/src/assets/locales/kk.png',
  };
  const links = [
    { link: '/registry', text: 'Registry' },
    { link: '/news', text: 'Blog' },
    { link: '/cases', text: 'Cases' },
    { link: '/smi', text: 'Media' },
    { link: '/consult', text: 'Consult' },
    { link: '/contacts', text: 'Contacts' },
  ];
  const [isNavActive, setIsNavActive] = useState(false);

  function toggleNav(): void {
    setIsNavActive(!isNavActive);
  }

  return (
    <>
      <MockBackground background={colors.globalBg} color={colors.textColor}>
        <Header colors={colors}>
          <Anchor href="/">
            <Logo colors={colors} src="https://ifkz.org/src/assets/logo.png" alt="Internet Freedom">
              Internet Freedom
              <br />
              <b>Kazakhstan</b>
            </Logo>
          </Anchor>
          <Butter colors={colors} onClick={toggleNav}>
            Menu
          </Butter>
          {isNavActive && (
            <>
              <button className="shade" onClick={toggleNav} />
              <Navigation>
                <NavigationItem colors={colors} href="/" className="navigation__link">
                  Home
                </NavigationItem>
                {links.map((item, i) => (
                  <NavigationItem
                    colors={colors}
                    href={item.link}
                    className="navigation__link"
                    key={i}
                  >
                    {item.text}
                  </NavigationItem>
                ))}
                <ThemeSwitch colors={colors} className="mt-4" isActive={true}>
                  Theme
                </ThemeSwitch>
                <LocaleSelect
                  className="mt-3"
                  languages={languages}
                  locale={Object.keys(languages)[0]}
                />
              </Navigation>
            </>
          )}
        </Header>
      </MockBackground>
    </>
  );
};
