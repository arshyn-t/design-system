import React, { FC } from 'react';

import { colors } from '../../../../../core/colors';
import { Container } from '../../../../atoms/Container';
import { MockBackground } from '../../../../atoms/MockBackground';
import { Logo } from './component';

export default {
  title: 'Organisms/Header',
};

export const LogoBasic: FC = () => (
  <>
    <Container mini>
      <MockBackground>
        <Logo colors={colors} src="https://ifkz.org/src/assets/logo.png" alt="Internet Freedom">
          Internet Freedom
          <br />
          <b>Kazakhstan</b>
        </Logo>
      </MockBackground>
    </Container>
  </>
);
