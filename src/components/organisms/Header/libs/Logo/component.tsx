import styled from '@emotion/styled';
import classNames from 'classnames';
import React, { FC } from 'react';

import { Props } from './props';

const LogoBase: FC<Props> = ({ className, src, alt, children, colors: _a, ...rest }: Props) => (
  <div className={classNames(className, 'logo--wrapper')} {...rest}>
    <img className="logo" src={src} alt={alt} />
    <span className="logo-title">{children}</span>
  </div>
);

export const Logo = styled(LogoBase)<Pick<Props, 'colors'>>`
  display: flex;
  align-items: center;
  .logo {
    display: block;
    height: 60px;
  }
  .logo-title {
    font-weight: 800;
    margin-left: 10px;
    line-height: 1;
    color: #fff;
    transition: color 0.2s;
  }
  .logo-title b {
    font-size: 9px;
    font-weight: 700;
    letter-spacing: 6px;
    text-transform: uppercase;
    background: ${({ colors }) => colors.accentBlue};
    color: #fff;
    margin-top: 6px;
    padding: 3px 3px 3px 11px;
    text-align: center;
    display: inline-block;
  }
`;
