import React, { FC } from 'react';

import { colors } from '../../../core/colors';
import { Anchor } from '../../atoms/Anchor';
import { Footer } from '../Footer';
import { FooterAbout } from '../FooterAbout';
import { FooterWrapper } from './component';

export default {
  title: 'Organisms/Footer',
};

export const FooterWrapperBasic: FC = () => {
  const socials = [
    {
      img: 'https://ifkz.org/src/assets/popular/instagram.png',
      url: 'https://instagram.com/internetfreedom_kz?igshid=g7nrax74t8te',
    },
    {
      img: 'https://ifkz.org/src/assets/popular/facebook.png',
      url: 'https://www.facebook.com/internetfreedomkz/',
    },
    {
      img: 'https://ifkz.org/src/assets/popular/telegram.png',
      url: 'https://t.me/InternetFreedomKZ',
    },
  ];
  return (
    <>
      <FooterWrapper>
        <FooterAbout title="О проекте">
          Проект разработан при финансовой поддержке Фонда Сорос-Казахстан. Содержание любой
          публикации отражает точку зрения автора/ов, которая не обязательно совпадает с точкой
          зрения Фонда Сорос-Казахстан.
        </FooterAbout>
        <Footer colors={colors}>
          <div className="socials__partners--wrapper">
            <Anchor href="/partners" className="social__links">
              Socials
            </Anchor>
            <Anchor href="/policy" className="social__links">
              Privacy
            </Anchor>
          </div>
          <div className="socials__info">
            <h3 className="title">Our social medias</h3>
            <div className="socials-grid">
              {socials.map((social, i) => (
                <div className="social" key={i}>
                  <Anchor href={social.url}>
                    <img src={social.img} alt="" className="social__img" />
                  </Anchor>
                </div>
              ))}
            </div>
          </div>
        </Footer>
      </FooterWrapper>
    </>
  );
};
