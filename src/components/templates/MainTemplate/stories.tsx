/** @jsx jsx */
import { jsx } from '@emotion/core';
import React, { FC } from 'react';

import { colors, colorsLight } from '../../../core/colors';
import { Anchor } from '../../atoms/Anchor';
import { Container } from '../../atoms/Container';
import { MockBackground } from '../../atoms/MockBackground';
import { MainTemplate } from './component';
import * as mock from './mock';

export default {
  title: 'Templates/Main',
};

export const DarkTheme: FC = () => (
  <React.Fragment>
    <MockBackground
      className="p-0"
      background={colors.globalBg}
      color={colors.textColor}
      css={{ height: '100vh' }}
    >
      <MainTemplate
        colors={colors}
        header={mock.header}
        footer={mock.footer}
        footerAbout={mock.footerAbout}
      >
        <Container>
          <Anchor href="/">Some mock title</Anchor>
        </Container>
      </MainTemplate>
    </MockBackground>
  </React.Fragment>
);

export const LightTheme: FC = () => (
  <React.Fragment>
    <MockBackground
      className="p-0"
      background={colorsLight.globalBg}
      color={colorsLight.textColor}
      css={{ height: '100vh' }}
    >
      <MainTemplate
        colors={colorsLight}
        header={mock.header}
        footer={mock.footer}
        footerAbout={mock.footerAbout}
      >
        <Container>
          <Anchor href="/">Some mock title</Anchor>
        </Container>
      </MainTemplate>
    </MockBackground>
  </React.Fragment>
);
