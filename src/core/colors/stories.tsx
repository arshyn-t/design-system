import React, { FC } from 'react';

import { MockBackground } from '../../components/atoms/MockBackground';
import { colors, colorsLight } from './colors';

export default {
  title: 'Mock/Colors',
};

export const DarkTheme: FC = () => (
  <>
    <div className="container">
      <MockBackground className="mb-2" background={colors.accentBlue} color={colors.textColor}>
        colors.accentBlue
      </MockBackground>
      <MockBackground className="mb-2" background={colors.accentBlueHover} color={colors.textColor}>
        colors.accentBlueHover
      </MockBackground>
      <MockBackground className="mb-2" background={colors.darkBg} color={colors.textColor}>
        colors.darkBg
      </MockBackground>
      <MockBackground className="mb-2" background={colors.globalBg} color={colors.textColor}>
        colors.globalBg
      </MockBackground>
    </div>
  </>
);

export const LightTheme: FC = () => (
  <>
    <div className="container">
      <MockBackground className="mb-2" background={colorsLight.accentBlue} color={colors.textColor}>
        colors.accentBlue
      </MockBackground>
      <MockBackground
        className="mb-2"
        background={colorsLight.accentBlueHover}
        color={colors.textColor}
      >
        colors.accentBlueHover
      </MockBackground>
      <MockBackground
        className="mb-2"
        background={colorsLight.darkBg}
        color={colorsLight.textColor}
      >
        colors.darkBg
      </MockBackground>
      <MockBackground
        className="mb-2"
        background={colorsLight.globalBg}
        color={colorsLight.textColor}
      >
        colors.globalBg
      </MockBackground>
    </div>
  </>
);
