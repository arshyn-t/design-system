/** @jsx jsx */
import { jsx } from '@emotion/core';
import classNames from 'classnames';
import React, { FC } from 'react';

import { Container } from '../../components/atoms/Container';
import { Heading } from '../../components/atoms/Heading';
import { MockBackground } from '../../components/atoms/MockBackground';
import { colors, colorsLight } from '../colors';
import { shadows } from './shadows';

export default {
  title: 'Mock/Shadows',
};

export const Box: FC = () => (
  <React.Fragment>
    <Container mini>
      <MockBackground
        className={classNames('mb-3')}
        background={colors.globalBg}
        css={{ boxShadow: shadows.Box.Wide }}
      >
        <Heading as="h1">Box.Wide</Heading>
      </MockBackground>
      <MockBackground
        className={classNames('mb-3')}
        background={colors.globalBg}
        css={{ boxShadow: shadows.Box.Close }}
      >
        <Heading as="h1">Box.Close</Heading>
      </MockBackground>
      <MockBackground
        className={classNames('mb-3')}
        background={colors.globalBg}
        css={{ boxShadow: shadows.Box.Platform }}
      >
        <Heading as="h1">Box.Platform</Heading>
      </MockBackground>
    </Container>
  </React.Fragment>
);

export const Text: FC = () => (
  <React.Fragment>
    <Container mini>
      <MockBackground
        className={classNames('mb-3')}
        background="#fff"
        color={colorsLight.textColor}
      >
        <Heading as="h1" css={{ textShadow: shadows.Text.Small }}>
          Text.Small
        </Heading>
      </MockBackground>
      <MockBackground
        className={classNames('mb-3')}
        background="#fff"
        color={colorsLight.textColor}
      >
        <Heading as="h1" css={{ textShadow: shadows.Text.Medium }}>
          Text.Medium
        </Heading>
      </MockBackground>
      <MockBackground
        className={classNames('mb-3')}
        background="#fff"
        color={colorsLight.textColor}
      >
        <Heading as="h1" css={{ textShadow: shadows.Text.Wide }}>
          Text.Wide
        </Heading>
      </MockBackground>
    </Container>
  </React.Fragment>
);
