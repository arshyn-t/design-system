# IFKZ Design System

Components pack used for IFKZ.org frontend

## Installation

Use the package manager [yarn](https://yarnpkg.com/) to install the package.

```bash
$ yarn add @ifkz/design-system
```

## Package Usage
Just import our components and use them.
```javascript 1.8
import { MockBackground } from '@ifkz/design-system'
<MockBackground />
```

For documentation or contribution just clone our github repository and run Storybook UI.
```bash
$ git clone https://gitlab.com/ifkz/design-system.git
$ yarn
$ yarn start
```


## License
[MIT](https://choosealicense.com/licenses/mit/)
